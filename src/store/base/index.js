//import state from './state'
//import * as getters from './getters'
//import * as mutations from './mutations'
//import * as actions from './actions'

export default {
  namespaced: true,
 // state,
//  getters,
//  mutations,
//  actions

state: {
    loggedUser: ''
  },
  mutations: {
    SET_LOGGEDUSER: (state, username) =>  state.loggedUser = username
  },
  actions: {
    setLoggerUser: ({commit, state}, username) => commit('SET_LOGGEDUSER', username ) 
  },
  getters: {
    getLoggerUser: state => state.loggedUser
  }
  
}


