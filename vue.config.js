module.exports = {
  pwa: {
    name: 'VueTest',
    themeColor: '#8D89FF',
    msTileColor: '#4A2FDD'
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  }
}
